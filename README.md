JIRA Mercurial Plugin
=====================

[ ![Codeship Status for servicerocket-labs/jira-mercurial-plugin](https://codeship.com/projects/90fc8e80-a085-0132-c300-3e3486fb28a9/status?branch=master)](https://codeship.com/projects/65434)

What is it?
-----------

This plugin adds a **Mercurial Commits** tab to JIRA issues, containing
commit logs associated with the issue.

For example, if your commit message is: "This fixes JRA-52 and JRA-54"
- the commit details would be displayed in a tab when viewing the issues JRA-52 and JRA-54.

The plugin works by updating a local clone of the repository you want
to monitor, and looking at all the change logs for messages related to
JIRA issues.

You can also generate release notes by providing a repository, a start
and an end revision. The changeset commit messages are scanned for
references to JIRA issues.

Build
-----
You need Apache Maven to build this project. One of dependencies, namely `jndi` needs to be installed manually on your
local Maven repository prior to building the project. You can download the `jndi` version `1.2.1` from 
[Java Platform Technology Downloads Page](http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-java-plat-419418.html)
and then install it using the following command:

    mvn install:install-file -DgroupId=jndi -DartifactId=jndi -Dversion=1.2.1 -Dpackaging=jar -Dfile=jndi-1.2.1.jar
    
For more information please refer to [this guide](https://maven.apache.org/guides/mini/guide-coping-with-sun-jars.html).

Quick Install Instructions
--------------------------

1.  Copy the plugin jar file into plugins/installed-plugins

2. If you are upgrading from an older version of the plugin, please
delete the Mercurial index directory. This is located wherever you
configured for JIRA indexes (Administration, Indexing) and is named
"*plugins/atlassian-mercurial-revisions*" or
"*plugins/mercurial-jira-revision*s".  The plugin will recreate this
directory when its service first runs.

2. Restart JIRA and check the log file for any related errors.

3. Go to Admin, Global Settings, Mercurial Repositories and add your
local repositories.

Optional Steps:

4. Create a test Mercurial repository

		`cd /tmp`

		`mkdir hg_repos`
	
		`cd hg_repos`

		`hg init a_local_repo`

		`cd a_local_repo`

		`hg serve -p 8000 --prefix test`

This creates an empty repository at *http://localhost:8000/test*

Make some changes to it (hg add, hg commit, hg push as usual) and
ensure that at least one commit message has a valid JIRA issue id in
it. If you have problems with push, see Troubleshooting.


Troubleshooting
---------------

1. The first time the service runs it will take a while to index all
of your existing issues. For remote repositories using ssh allow a few
seconds per repository.

2. Edit atlassian-jira/WEB-INF/classes/log4j.properties and add lines
   to enable logging for a particular area of the plugin,
   e.g. Mercurial commands use the class name HGRepository

	`log4j.category.com.consultingtoolsmiths.hg.core.io.HGRepository = DEBUG, console, filelog`
`log4j.additivity.com.consultingtoolsmiths.hg.core.io.HGRepository = false`

	Other classes that are used for useful log areas include:
	<code>
com.consultingtoolsmiths.jira.plugin.ext.mercurial.revisions.RevisionIndexer
com.consultingtoolsmiths.jira.plugin.ext.mercurial.reports.ReportsResource
	</code>

	Version 5.0 and above:
<code>net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexer
net.customware.jira.plugins.ext.mercurial.reports.ReportsResource
</code>

	**Note**: leaving logs on DEBUG level can create large logs in a short
time due to the service running periodically.

3. Make sure that mercurial is installed on your JIRA server and can
be run without any user interaction by the userid that is running
JIRA. The hg pull logic assumes that the userid running jira can run
pull without a password or that the password has already been set
up.

4. Changing the interval of the Mercurial Revision Index Service
(Admin, System, Services) forces it to run immediately, which is
useful for testing changes. Update the period (in minutes) to whatever
you want, but not less than (5*number of repositories) seconds if you
are using remote repositories and ssh

5. Clones get pulled automatically but not updated, so don't expect to see the
   changes directly in the hg_clones directory. You can however use hg tip
   in that repository to see whether it has been updated. You can also
   prepopulate your clones directory so that JIRA doesn't have to
   clone, just hg pull.

	The logic for creating and updating clones is:

	a. If the hg clones directory doesn't exist, create it.
	b. If the specific cloned repository doesn't exist, clone it with "hg
   clone". This can take some time for large repositories, so is worth
   doing before JIRA starts up.
	c. Update the clone with "hg pull" whenever the service runs, which
   is every 60 minutes by default. Note that no update (-	u) occurs.

6. If hg push gives an error "ssl required" add a file named hgrc to the directory /tmp/hg_repos/a_local_repo/.hg containing:

		[web]	
		push_ssl = false	
		allow_push = 
		

	and restart the hg serve command.

	While you're at it, you may also want to add a line such as:

		[ui]
		username = Roget Rabet <rrabet@pobox.com>
	
	to your own local ~/.hgrc file. Create it if it doesn't already exist. This is the name that Mercurial commits and pushes changesets as.

7. More specific notes are available about generating release notes in
src/main/java/com/consultingtoolsmiths/jira/plugin/ext/mercurial/reports/notes.txt

Building the Plugin
-------------------

Use Atlassian PDK and "atlas-package" as usual to produce the jar file
in the target directory.

History
-------

**4.1.0** Matt Doar at Consulting Toolsmiths added:

* support for bulk repository operations
* using mercurial tags as start and end points for release notes
* a new project tab to make generating release notes easier for users
* various bug fixes

**4.0.5** Matt Doar at Consulting Toolsmiths upgraded the pom to v2 and
added support for automatic release note generation.

**4.0.1** Matt Doar at Consulting Toolsmiths used the JIRA Subversion
plugin version 10.5.2 as the basis for a new version of the Mercurial
plugin. This provides administration of repositories from the JIRA
Admin menu and removes the need for the
mercurial-jira-plugin.properties file. This work was sponsored by
Intercontinental Exchange.

**4.0** Matt Doar at Consulting Toolsmiths examined the changes to the
Subversion plugin between 10.4.2 and 10.5.2 to check for necessary
changes for this plugin for JIRA 4.0. Reviewed the list of issues fixed as well.

**3.13** Matt Doar at Consulting Toolsmiths removed unused files and
changed the domain to com.consultingtoolsmiths. The version number now
tracks the major version of JIRA. This version was tested with
Mercurial 1.3.1 on OSX 10.5.

**0.2** (sic) Chad Loder and Vladislav Manchev. This version brought the
plugin up to date for maven2 and JIRA 3.12, JIRA 3.13. These changes
now require JDK1.5 to compile.  JHG-1 and JHG-5 changes incorporated.

**0.3.7.0.7** Matt Doar. Updated version to reflect the name change of the
plugin. No functionality changed in this release.

**0.3.7.0.6** Mihai Ibanescu. Modified some more, tested against Jira 3.7.1

**0.3.7.0.5** Mihai Ibanescu. Modified to work with Jira 3.7. Untested.

**0.3.6.0.4** Mihai Ibanescu. Modified to work with Jira 3.6, tested against Jira 3.6.5.

**0.2** Matt Doar. Support for multiple repositories added

**0.1** Matt Doar at XenSource. First release based on the atlassian
Subversion plugin v.0.8.2 and the javasvn library v 
**0.8.8**, with some
help parsing mercurial log output from the scm project.

More
----

Suggestions, bug reports or feature requests?

    http://studio.plugins.atlassian.com/browse/JHG

