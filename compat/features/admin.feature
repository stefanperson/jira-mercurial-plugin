Feature: Add new repository
Scenario: In order to enable users to view mercurial commit message in an issue
  As a JIRA Admin
  I want to add a new mercurial repository with the following parameters: project, issue type and issue link type

Given I am a JIRA Admin
  When I add a new repository that located at "http://localhost:8888/test"
  Then I should see the repository added and activated