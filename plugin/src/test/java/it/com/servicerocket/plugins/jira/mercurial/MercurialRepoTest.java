package it.com.servicerocket.plugins.jira.mercurial;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import it.com.servicerocket.plugins.jira.mercurial.pageObject.MercurialRepoPage;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MercurialRepoTest {

    public final static JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class);


    @Test public void pluinShouldBeInstalled() {
        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");

        MercurialRepoPage page = jira.visit(MercurialRepoPage.class);

        String title =  page.getTitleText();

        assertThat(title, is("Mercurial Repositories"));

    }
}
