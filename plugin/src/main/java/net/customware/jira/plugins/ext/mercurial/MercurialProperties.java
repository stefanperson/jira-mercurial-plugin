package net.customware.jira.plugins.ext.mercurial;

public class MercurialProperties implements HgProperties {
    private String root;
    private String clonedir;
    private String releasenotesemail;
    private String releasenoteslatest;
    private String subreposcmd;
    private String displayName;
    private String username;
    private String password;
    private Boolean subrepos;
    private Boolean revisionIndexing;
    private Integer revisioningCacheSize;
    private String privateKeyFile;
    private String changeSetFormat;
    private String webLinkType;
    private String fileModifiedFormat;
    
    public void fillPropertiesFromOther(HgProperties other) {
        if (this.getClonedir() == null) {
            this.clonedir = other.getClonedir();
        }
        if (this.getReleasenotesemail() == null) {
            this.releasenotesemail = other.getReleasenotesemail();
        }
        if (this.getReleasenoteslatest() == null) {
            this.releasenoteslatest = other.getReleasenoteslatest();
        }
        if (this.getSubreposcmd() == null) {
            this.subreposcmd = other.getSubreposcmd();
        }
        this.subrepos = other.getSubrepos();
        if (this.getUsername() == null) {
            this.username = other.getUsername();
        }
        if (this.getPassword() == null) {
            this.password = other.getPassword();
        }
        if (this.getPrivateKeyFile() == null) {
            this.privateKeyFile = other.getPrivateKeyFile();
        }
        if (this.getRevisionIndexing() == null) {
            this.revisionIndexing = other.getRevisionIndexing();
        }
        if (this.getRevisionCacheSize() == null) {
            this.revisioningCacheSize = other.getRevisionCacheSize();
        }
        
        if (webLinkType == null)
            setWebLinkType(other.getWebLinkType());
        
        if (changeSetFormat == null)
            setChangeSetFormat(other.getChangesetFormat());
        
        if (fileModifiedFormat == null)
            setFileModifiedFormat(other.getFileModifiedFormat());
    }
    
    public String getWebLinkType() {
        return webLinkType;
    }
    
    public void setWebLinkType(String webLinkType) {
        this.webLinkType = webLinkType;
    }
    
    public String toString() {
        return "clonedir: " + clonedir + " subrepos: " + getSubrepos() + " username: " + getUsername() + " password: " + getPassword() + " privateKeyFile: " + getPrivateKeyFile() + " revisioningIndex: " + getRevisionIndexing() + " revisioningCacheSize: " + getRevisionCacheSize() + ", subreposcmd: " + getSubreposcmd() + ", releasenotesemail: " + getReleasenotesemail() + ", releasenoteslatest: " + getReleasenoteslatest();
    }
    
    public String getRoot() {
        return root;
    }
    
    public String getClonedir() {
        return clonedir;
    }

    public String getReleasenotesemail() {
        return releasenotesemail;
    }

    public String getReleasenoteslatest() {
        return releasenoteslatest;
    }

    public String getSubreposcmd() {
        return subreposcmd;
    }
    
    public Boolean getSubrepos() {
        return subrepos;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public Boolean getRevisionIndexing() {
        return revisionIndexing;
    }
    
    public Integer getRevisionCacheSize() {
        return revisioningCacheSize;
    }
    
    public String getPrivateKeyFile() {
        return privateKeyFile;
    }
    
    public String getChangesetFormat() {
        return changeSetFormat;
    }
    
    public String getFileModifiedFormat() {
        return fileModifiedFormat;
    }

    public MercurialProperties setRoot(String root) {
        this.root = root;
        return this;
    }
    
    public MercurialProperties setClonedir(String clonedir) {
        this.clonedir = clonedir;
        return this;
    }
    
    public MercurialProperties setReleasenotesemail(String value) {
        this.releasenotesemail = value;
        return this;
    }
    
    public MercurialProperties setReleasenoteslatest(String value) {
        this.releasenoteslatest = value;
        return this;
    }
    
    public MercurialProperties setSubreposcmd(String subreposcmd) {
        this.subreposcmd = subreposcmd;
        return this;
    }
    
    public MercurialProperties setSubrepos(Boolean subrepos) {
        this.subrepos = subrepos;
        return this;
    }
    
    public MercurialProperties setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
    
    public MercurialProperties setUsername(String username) {
        this.username = username;
        return this;
    }
    
    public MercurialProperties setPassword(String password) {
        this.password = password;
        return this;
    }
    
    public MercurialProperties setRevisionIndexing(Boolean revisionIndexing) {
        this.revisionIndexing = revisionIndexing;
        return this;
    }
    
    public MercurialProperties setRevisioningCacheSize(Integer revisioningCacheSize) {
        this.revisioningCacheSize = revisioningCacheSize;
        return this;
    }
    
    public MercurialProperties setPrivateKeyFile(String privateKeyFile) {
        this.privateKeyFile = privateKeyFile;
        return this;
    }
    
    public MercurialProperties setChangeSetFormat(String changeSetFormat) {
        this.changeSetFormat = changeSetFormat;
        return this;
    }
    
    public MercurialProperties setFileModifiedFormat(String fileModifiedFormat) {
        this.fileModifiedFormat = fileModifiedFormat;
        return this;
    }
}
