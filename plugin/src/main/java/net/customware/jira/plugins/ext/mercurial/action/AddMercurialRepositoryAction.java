package net.customware.jira.plugins.ext.mercurial.action;

import net.customware.jira.plugins.ext.mercurial.MercurialManager;
import net.customware.jira.plugins.ext.mercurial.HgProperties;

import com.opensymphony.util.TextUtils;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.util.PathUtils;

import java.io.File;

public class AddMercurialRepositoryAction extends MercurialActionSupport implements HgProperties {
    protected String root;
    protected String clonedir = "";
    protected String displayName;
    protected String username;
    protected String password;
    protected String privateKeyFile;
    protected Boolean revisionIndexing = Boolean.TRUE;
    protected Boolean subrepos = Boolean.FALSE;
    protected Integer revisionCacheSize = new Integer(10000);
    protected String webLinkType;
    protected String changesetFormat;
    protected String fileModifiedFormat;
    protected String subreposcmd = ""; // only used for subrepositories
    protected String releasenotesemail = "";
    protected String releasenoteslatest = "";
    
    protected final JiraHome jiraHome;
    
    public AddMercurialRepositoryAction(JiraHome jiraHome) {
        this.jiraHome = jiraHome;
    }
    
    public void doValidation() {
        if (!TextUtils.stringSet(getDisplayName())) {
            addError("displayName", getText("mercurial.errors.you.must.specify.a.name.for.the.repository"));
        }
        
        validateRepositoryParameters();
    }
    
    public String getRoot() {
        return root;
    }
    
    public void setRoot(String root) {
        this.root = root != null ? root.trim() : root;
    }
    
    public String getSubreposcmd() {
        return subreposcmd;
    }
    
    public void setSubreposcmd(String subreposcmd) {
        this.subreposcmd = subreposcmd;
    }
    
    public String getClonedir() {
        return clonedir;
    }
    
    public void setClonedir(String clonedir) {
        // Always an absolute location
        // If empty, convert to a default location
        clonedir = clonedir.trim();
        // If relative, prefix with ${jira.home}/caches/hg_clones"
        if (clonedir.equals("")) {
            String CLONES_DIR = PathUtils.joinPaths("caches", "hg_clones");
            clonedir = new File(jiraHome.getHomePath(), CLONES_DIR).getAbsolutePath();
        }
        this.clonedir = clonedir;
    }
    
    public String getReleasenotesemail() {
        return releasenotesemail;
    }
    
    public void setReleasenotesemail(String value) {
        this.releasenotesemail = value;
    }
    
    public String getReleasenoteslatest() {
        return releasenoteslatest;
    }
    
    public void setReleasenoteslatest(String value) {
        this.releasenoteslatest = value;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        if (TextUtils.stringSet(username)) {
            this.username = username;
        } else {
            this.username = null;
        }
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        if (TextUtils.stringSet(password)) {
            this.password = password;
        } else {
            this.password = null;
        }
    }
    
    public Boolean getSubrepos() {
        return subrepos;
    }
    
    public void setSubrepos(Boolean subrepos) {
        this.subrepos = subrepos;
    }
    
    public Boolean getRevisionIndexing() {
        return revisionIndexing;
    }
    
    public void setRevisionIndexing(Boolean revisionIndexing) {
        this.revisionIndexing = revisionIndexing;
    }
    
    public Integer getRevisionCacheSize() {
        return revisionCacheSize;
    }
    
    public void setRevisionCacheSize(Integer revisionCacheSize) {
        this.revisionCacheSize = revisionCacheSize;
    }
    
    public String getPrivateKeyFile() {
        return privateKeyFile;
    }
    
    public void setPrivateKeyFile(String privateKeyFile) {
        if (TextUtils.stringSet(privateKeyFile)) {
            this.privateKeyFile = privateKeyFile;
        } else {
            this.privateKeyFile = null;
        }
    }
    
    public String getWebLinkType() {
        return webLinkType;
    }
    
    public void setWebLinkType(String webLinkType) {
        this.webLinkType = webLinkType;
    }
    
    public String getChangesetFormat() {
        return changesetFormat;
    }
    
    public void setChangesetFormat(String changesetFormat) {
        if (TextUtils.stringSet(changesetFormat)) {
            this.changesetFormat = changesetFormat;
        } else {
            this.changesetFormat = null;
        }
    }
    
    public String getFileModifiedFormat() {
        return fileModifiedFormat;
    }
    
    public void setFileModifiedFormat(String fileModifiedFormat) {
        if (TextUtils.stringSet(fileModifiedFormat)) {
            this.fileModifiedFormat = fileModifiedFormat;
        } else {
            this.fileModifiedFormat = null;
        }
    }
        
    public String doExecute() throws Exception {
        return commonAction(false);
    }

    public String doAddsubrepos() throws Exception {
        return commonAction(true);
    }

    protected String commonAction(boolean isSubrepos) throws Exception {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        
        /* 
         * This is a way to gather enough information about the current contents of 
         * a directory of repositories to make sure that they are added to
         * JIRA if new, and deactivated if no longer present.
         */
        if (isSubrepos) {
            log.info("Treating repository as subrepositories: " + getRoot());
            setSubrepos(true);
            setRevisionIndexing(false);
        }
        
        MercurialManager mercurialManager = getMultipleRepoManager().createRepository(this, isSubrepos);
        if (!mercurialManager.isActive()) {
            addErrorMessage(mercurialManager.getInactiveMessage());
            addErrorMessage(getText("admin.errors.occured.when.creating"));
            getMultipleRepoManager().removeRepository(mercurialManager.getId());
            return ERROR;
        }
        
        return getRedirect("ViewMercurialRepositories.jspa");
    }
    
    // This is public for testing purposes
    public void validateRepositoryParameters() {
        if (!TextUtils.stringSet(getDisplayName()))
            addError("displayName", getText("mercurial.errors.you.must.specify.a.name.for.the.repository"));
        if (!TextUtils.stringSet(getRoot()))
            addError("root", getText("mercurial.errors.you.must.specify.the.root.of.the.repository"));
        // cloneDir can be empty to use the default location
    }
    
}
