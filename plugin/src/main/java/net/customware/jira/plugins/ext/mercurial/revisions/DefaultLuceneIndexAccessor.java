package net.customware.jira.plugins.ext.mercurial.revisions;

import com.atlassian.jira.issue.index.IndexException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;

/**
 * An implementation that uses JIRA's LuceneUtils for its guts.
 *
 * @since 0.9.12
 */
class DefaultLuceneIndexAccessor
{

    public IndexReader getIndexReader(String path) throws IndexException
    {
        return LuceneUtils.getIndexReader(path);
    }

    public IndexWriter getIndexWriter(String path, boolean create, StandardAnalyzer analyzer) throws IndexException
    {
        return LuceneUtils.getIndexWriter(path, create, analyzer);
    }
}
