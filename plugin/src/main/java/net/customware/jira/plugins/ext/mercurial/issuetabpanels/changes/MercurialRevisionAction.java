/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Sep 16, 2004
 * Time: 2:00:52 PM
 */
package net.customware.jira.plugins.ext.mercurial.issuetabpanels.changes;

import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;
import net.customware.hg.core.io.HGLogEntry;
import net.customware.hg.core.io.HGLogEntryPath;
import net.customware.jira.plugins.ext.mercurial.MercurialConstants;
import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManager;
import net.customware.jira.plugins.ext.mercurial.linkrenderer.MercurialLinkRenderer;
import net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.*;

/**
 * One item in the 'Mercurial Commits' tab.
 */
public class MercurialRevisionAction extends AbstractIssueAction
{
    private static Logger log = Logger.getLogger(MercurialRevisionAction.class);
    private final HGLogEntry revision;
    private final long repoId;
    protected final IssueTabPanelModuleDescriptor descriptor;
    protected MultipleMercurialRepositoryManager multipleMercurialRepositoryManager;
    protected Timestamp timePerformed;

    public MercurialRevisionAction(HGLogEntry logEntry, MultipleMercurialRepositoryManager multipleMercurialRepositoryManager, IssueTabPanelModuleDescriptor descriptor, long repoId)
    {
        super(descriptor);
        this.multipleMercurialRepositoryManager = multipleMercurialRepositoryManager;
        this.descriptor = descriptor;
        /* issue SVN-93 */
        this.revision = new HGLogEntry(
                logEntry.getChangedPaths(),
                logEntry.getShortRevision(),
                logEntry.getFullRevision(),
                logEntry.getAuthor(),
                logEntry.getDate(),
                logEntry.getTags(),
                logEntry.getBranch(),
                rewriteLogMessage(logEntry.getMessage()));
        this.timePerformed = new Timestamp(revision.getDate().getTime());
        this.repoId = repoId;
    }

    protected void populateVelocityParams(Map params)
    {

        params.put("stringUtils", new StringUtils());
        params.put("hg", this);

    }

    public MercurialLinkRenderer getLinkRenderer()
    {
        return multipleMercurialRepositoryManager.getRepository(repoId).getLinkRenderer();
    }

    public String getRepositoryDisplayName()
    {
        return multipleMercurialRepositoryManager.getRepository(repoId).getDisplayName();
    }

    public Date getTimePerformed()
    {
        return timePerformed;
    }

    public long getRepoId()
    {
        return repoId;
    }

    public String getUsername()
    {
        return revision.getAuthor();
    }

    public HGLogEntry getRevision()
    {
        return revision;
    }

    public boolean isAdded(HGLogEntryPath logEntryPath)
    {
        return MercurialConstants.ADDED == logEntryPath.getType();
    }

    public boolean isModified(HGLogEntryPath logEntryPath)
    {
        return MercurialConstants.MODIFICATION == logEntryPath.getType();
    }

    public boolean isReplaced(HGLogEntryPath logEntryPath)
    {
        return MercurialConstants.REPLACED == logEntryPath.getType();
    }

    public boolean isDeleted(HGLogEntryPath logEntryPath)
    {
        return MercurialConstants.DELETED == logEntryPath.getType();
    }

    /**
     * Converts all lower case JIRA issue keys to upper case so that they can be
     * correctly rendered in the Velocity macro, makelinkedhtml.
     *
     * @param logMessageToBeRewritten
     * The HG log message to be rewritten.
     * @return
     * The rewritten HG log message.
     * @see
     * <a href="http://jira.atlassian.com/browse/SVN-93">issue SVN-93</a>
     */
    protected String rewriteLogMessage(final String logMessageToBeRewritten)
    {
        String logMessage = logMessageToBeRewritten;
        final String logMessageUpperCase = StringUtils.upperCase(logMessage);
        final Set <String>issueKeys = new HashSet<String>(getIssueKeysFromCommitMessage(logMessageUpperCase));

        for (String issueKey : issueKeys)
            logMessage = logMessage.replaceAll("(?ium)" + issueKey, issueKey);

        return logMessage;
    }

    List<String> getIssueKeysFromCommitMessage(String logMessageUpperCase)
    {
        return RevisionIndexer.getIssueKeysFromString(logMessageUpperCase);
    }
}
