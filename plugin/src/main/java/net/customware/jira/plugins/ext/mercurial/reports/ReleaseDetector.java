package net.customware.jira.plugins.ext.mercurial.reports;

import com.atlassian.jira.util.ErrorCollection;

public interface ReleaseDetector {

    /**
     *
     * @return
     */
    public long getLatestRelease(ErrorCollection errors);

    /**
     *
     * @return
     */
    public long getPreviousRelease(ErrorCollection errors);

    /**
     *
     * @return
     */
    public String getVersion(long changeset);

    /**
     *
     * @return true is the given String is a release marker
     */
    public boolean isMarker(String msg);

    /**
     * If an artifact id is specified, it will be used.  If a release
     * can be detected, the artifact in the latest release will be
     * used.  If no release can be detected, use the repository
     * manager's display name.
     *
     * @return the current artifact id
     */
    public String getLatestArtifactId();
}
