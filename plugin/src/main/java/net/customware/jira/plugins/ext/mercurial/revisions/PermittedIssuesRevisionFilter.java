package net.customware.jira.plugins.ext.mercurial.revisions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.PermissionManager;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.util.OpenBitSet;

import java.io.IOException;
import java.util.Set;

public class PermittedIssuesRevisionFilter extends AbstractRevisionFilter
{
    private final Set<String> permittedIssueKeys;

    public PermittedIssuesRevisionFilter(IssueManager issueManager, PermissionManager permissionManager, User user, Set<String> permittedIssueKeys)
    {
        super(issueManager, permissionManager, user);
        this.permittedIssueKeys = permittedIssueKeys;
    }

	@Override
	public DocIdSet getDocIdSet(IndexReader reader) throws IOException {
	//public BitSet bits(IndexReader indexReader) throws IOException
		OpenBitSet bitSet = new OpenBitSet(reader.maxDoc());

		for (String issueKey : permittedIssueKeys) {
			TermDocs termDocs = reader.termDocs(new Term(RevisionIndexer.FIELD_ISSUEKEY, issueKey));

			while (termDocs.next()) {
				bitSet.set(termDocs.doc());
			}
		}

		return bitSet;
	}
}
