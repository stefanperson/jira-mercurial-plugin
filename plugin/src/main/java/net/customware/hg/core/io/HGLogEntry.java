/*
 * ====================================================================
 * Copyright (c) 2004 TMate Software Ltd.  All rights reserved.
 *
 * This software is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at http://tmate.org/svn/license.html.
 * If newer versions of this license are posted there, you may use a
 * newer version instead, at your option.
 * ====================================================================
 */

package net.customware.hg.core.io;

import com.atlassian.velocity.htmlsafe.HtmlSafe;

import java.util.Date;
import java.util.Map;

/**
 * @author Alexander Kitaev
 */
public class HGLogEntry {
    
    private long myShortRevision;
    private String myFullRevision;
    private String myAuthor;
    private Date myDate;
    private String myMessage;
    private Map myChangedPaths;
    private String myTags;
    private String myBranch;
    
    public HGLogEntry(Map changedPaths, long shortRevision, String fullRevision, String author, Date date, String tags, String branch, String message) {
        myShortRevision = shortRevision;
        myFullRevision = fullRevision;
        myAuthor = author;
        myDate = date;
        myMessage = message;
        myTags = tags;
        myBranch = branch;
        myChangedPaths = changedPaths;
    }
    
    public Map getChangedPaths() {
        return myChangedPaths;
    }
    
    public String getAuthor() {
        return myAuthor;
    }

    public String getTags() {
        return myTags;
    }

    public String getBranch() {
        return myBranch;
    }
    
    public Date getDate() {
        return myDate;
    }

    /**
     * Replaces spaces with non-breaking spaces for HTML display
     */
    @HtmlSafe
    public String getDatestr()
    {
        return myDate.toString().replaceAll(" ", "&nbsp;");
    }

    public String getMessage() {
        return myMessage;
    }
    public long getShortRevision() {
        return myShortRevision;
    }
    public String getFullRevision() {
        return myFullRevision;
    }
}
